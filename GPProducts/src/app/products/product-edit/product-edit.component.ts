import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  productForm: FormGroup;
  submitted = false;
  productId:string;

  constructor(
    private db: AngularFirestore,
    private formBuilder: FormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute,) { }

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      id: 0,
      name:['', Validators.required],
      price:[0, Validators.required],
      cost:[0, Validators.required],
      quantity:[0, Validators.required]
    });

    this.activeRoute.params.subscribe(params => {
      if(params["id"] == undefined || params["id"] == 0){
        alert('Parametro no definido');
        this.backToList();
      }

      this.productId = params["id"];
      
      this.db.doc<any>('products' + '/' + this.productId)
      .valueChanges()
      .subscribe((res) => {
        this.productForm.setValue({
          id: this.productId,
          name: res.name,
          price: res.price,
          cost: res.cost,
          quantity: res.quantity
        })
      })      
    });
  }

  onSubmit(){
    this.submitted = true;
    if(this.productForm.invalid){
      return;
    }

    this.db.doc('products/' + this.productId)
    .update(this.productForm.value)
    .then((res) => {
      alert('registros actualizados.');
      this.backToList();
    });
  }

  backToList(){
    this.productForm.reset();
    this.router.navigate(["../product-list"]);
  }
}
