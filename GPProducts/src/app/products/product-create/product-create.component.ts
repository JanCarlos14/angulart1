import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {

  productForm: FormGroup;
  submitted = false;

  constructor(
    private db: AngularFirestore,
    private formBuilder: FormBuilder,
    private router: Router,) { }

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      ID: 0,
      name:['', Validators.required],
      price:[0, Validators.required],
      cost:[0, Validators.required],
      quantity:[0, Validators.required]
    });
  }

  onSubmit(){
    this.submitted = true;
    if(this.productForm.invalid){
      return;
    }

    this.db.collection('products')
    .add(this.productForm.value)
    .then((res) => {
      alert('registros insertados.');
      this.backToList();
    });
  }

  backToList(){
    this.productForm.reset();
    this.router.navigate(["../product-list"]);
  }
}
