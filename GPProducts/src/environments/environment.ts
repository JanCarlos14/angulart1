// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCOjD9IhfD5KXBY_9pbAVE2gTltvK0BQNE",
    authDomain: "examenjancarlos.firebaseapp.com",
    databaseURL: "https://examenjancarlos-default-rtdb.firebaseio.com",
    projectId: "examenjancarlos",
    storageBucket: "examenjancarlos.appspot.com",
    messagingSenderId: "587562798970",
    appId: "1:587562798970:web:db9b7cd7e08e1afc91d0c3",
    measurementId: "G-RY67MS2NPJ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
